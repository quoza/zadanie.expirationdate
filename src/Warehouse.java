import java.time.LocalDateTime;
import java.util.*;

class Warehouse {
    private Queue<Product> listaProduktow = new PriorityQueue<>();

    void addProduct(Product product) {
        boolean check = true;
        for (Product p : listaProduktow){
            if (p.getName().equals(product.getName())){
                p.setPrice(product.getPrice());
            }

            if (p.getExpirationDate().equals(product.getExpirationDate())){
                p.setQuantity(p.getQuantity() + product.getQuantity());
                check = false;
            }
        }

        if (check) {
            listaProduktow.offer(product);
        }
    }

    void removeProduct(String name) throws Exception {
        boolean checker = false;
        for (Product product : listaProduktow){
            if (product.getName().equals(name)){
                listaProduktow.remove(product);
                checker = true;
            }
        }
        if (!checker){
            throw new Exception("Brak produktu o takiej nazwie.");
        }
    }

    private Product getProductByName (String name){
        for (Product product : listaProduktow){
            if (product.getName().equals(name)){
                return product;
            }
        }
        return null;
    }

    void checkProduct(){
        for (Product product : listaProduktow){
            System.out.print(product);
            if(LocalDateTime.now().isAfter(product.getExpirationDate())){
                System.out.println(" - przeterminowany.");
            } else {
                System.out.println();
            }
        }
    }
}