import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        Warehouse warehouse = new Warehouse();
        System.out.println("Expiration Date Program.");
        System.out.println("Command list: ");
        System.out.println("~ dodaj nazwa cena ilosc [termin przydatnosci]");
        System.out.println("~ usun nazwa");
        System.out.println("~ sprawdz");
        System.out.println("~ wyjdz");
        System.out.println();
        String commandUntrimmed;

        do{
            System.out.print("~ ");
            commandUntrimmed = sc.nextLine();
            String[] command = commandUntrimmed.split(" ");
            Wybor wybor = Wybor.valueOf(command[0].toUpperCase());

            if (command[0] == null || command[0].trim().isEmpty()) {continue;}
            if (wybor == Wybor.WYJDZ) {break;}

            switch (wybor) {
                case DODAJ:
                    double cena = Double.parseDouble(command[2]);
                    int ilosc = Integer.parseInt(command[3]);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
                    LocalDateTime expirationDate = LocalDateTime.parse(command[4] + " " + command[5], formatter);
                    Product product = new Product(command[1], cena, ilosc, expirationDate);
                    warehouse.addProduct(product);
                    break;
                case USUN:
                    try {
                        warehouse.removeProduct(command[1]);
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case SPRAWDZ:
                    warehouse.checkProduct();
                    break;
                case WYJDZ:
                    System.out.println("Zla komenda. Sprobuj ponownie.");
                    break;
            }

        } while (!commandUntrimmed.equals("wyjdz"));
    }
}