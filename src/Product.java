import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Product implements Comparable{
    private String name;
    private double price;
    private LocalDateTime expirationDate;
    private int quantity;

    public Product(String name, double cena, int quantity, LocalDateTime expirationDate) {
        this.name = name;
        this.price = cena;
        this.quantity = quantity;
        this.expirationDate = expirationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        return name + " = " + price + " zl, " + "ilosc: " + quantity +
                ", data waznosci: " + expirationDate.format(formatter) + ".";
    }

    @Override
    public int compareTo(Object o){
        if(o instanceof Product){
            return expirationDate.compareTo(((Product)o).expirationDate);
        }
        return 0;
    }
}